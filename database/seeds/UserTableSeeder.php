<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'petar',
            'email' => 'petargigant@live.com',
            'password' => bcrypt('admin'),
        ]);

        DB::table('users')->insert([
            'name' => 'Vlatko',
            'email' => 'gigantmk@yahoo.com',
            'password' => bcrypt('admin'),
        ]);
    }
}
