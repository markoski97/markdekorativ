@extends('NewDesign.template')

@section('title','Lajsni')
@section('_pagetitle')
    <section class="page-title" style="background-image:url(images/title/2.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-8 col-xs-12">
                    <h1>Лајсни</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Почетна</a></li>
                        <li class="active">Лајсни</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('create-content')
    <br>

        <div class="row padding">
            @auth
                <div class="col-md-2 offset-10 float-right">
                    <a href="{{route("lajsni.create")}}" class="btn btn-info float-right">Лајсни</a>
                </div>
            @endauth
        </div>
        <br>
        <section class="">
            <div class="auto-container">
                <div class="row clearfix">
                    @if($lajsni->count())
                        <div class="sec-title centered">
                            <h2>Лајсни</h2>
                            <img src="{{asset('img/lajsni1.jpg')}}" style="height:400px;width:500px">
                        </div>
                        <br>
                        <br>
                </div>
            </div>
        </section>

        <section class="project-section" style="padding-top:0px ">
            <div class="auto-container">
                {{--Ovde oda Filtrite--}}
                <div class="sortable-masonry">
                    <div class="filters clearfix">
                        <ul class="filter-tabs filter-btns clearfix">
                            <li class="active filter" data-role="button" data-filter=".all">Сите</li>
                                <li class="filter" data-role="button" data-filter=".Pvc-Со-кабел-канал">PVC Со Канал</li>
                                <li class="filter" data-role="button" data-filter=".Pvc-Без-кабел-канал">PVC Без Kанал</li>
                                <li class="filter" data-role="button" data-filter=".Медијапански">Медијапански</li>

                        </ul>
                    </div>

                    <div class="items-container row clearfix">
                    @foreach($lajsni as $lajsnis)
                            <!--Gallery Block-->
                            <div class="gallery-block masonry-item all {{$lajsnis->type}} col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="{{asset('uploads/lajsni/'.$lajsnis->image)}}"  alt=""/>
                                        <div class="overlay-box">
                                            <div class="content clearfix">
                                                <div class="pull-left">
                                                    <h3> <a href="{{'uploads/lajsni/'.$lajsnis->image}}" class="plus-icon lightbox-image"
                                                            data-fancybox-group="gallery-one">{{$lajsnis->title}}</a></h3>
                                                </div>
                                                <div class="pull-right">
                                                    <a href="{{'uploads/lajsni/'.$lajsnis->image}}" class="plus-icon lightbox-image"
                                                       data-fancybox-group="gallery-one"><span
                                                            class="icon fa fa-plus"></span></a>

                                                    @auth
                                                        <a href="{{'lajsni/'.$lajsnis->id}}"
                                                                class="icon fa fa-minus"></a>
                                                        @endauth
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    @endforeach
                    </div>
                    <div class="text-center">
                        {!! $lajsni->links()!!}

                    </div>
                </div>
            </div>
            @else
                <div class="container-fluid padding">
                    <div class="col-md-12">
                        <h1 class="text-center">Во Моментов Нема Производи Во Оваа Категорија.</h1>
                    </div>
                    @endif
                </div>
        </section>




@endsection

