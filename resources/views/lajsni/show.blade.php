@extends ('NewDesign.template')

@section('title','Akcii-show')
@section('_pagetitle')
    <section class="page-title" style="background-image:url(../images/title/2.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-8 col-xs-12">
                    <h1>{{$lajsni->title}}</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Почетна</a></li>
                        <li class="active">Лајсни</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('create-content')
    <br>
    <!--Sidebar Page Container-->
    <div class="">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <aside class="sidebar default-sidebar with-padding">
                        <!-- Sidebar Category -->
                        @auth
                            <div class="well">
                                <dl class="dl-horizontal">

                                    <dt>Креирано на:</dt>
                                    <dd>{{date('M j, Y H:i',strtotime($lajsni->created_at))}}</dd>
                                </dl>

                                <dl class="dl-horizontal">
                                    <dt>Последна промена на:</dt>
                                    <dd>{{date('M j, Y H:i',strtotime($lajsni->updated_at))}}</dd>
                                </dl>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12">

                                        {!!Form::open(['route'=>['lajsni.destroy',$lajsni->id],'method'=>'DELETE','enctype'=>"multipart/form-data"])!!}

                                        {!!Form::submit('Избриши',['class'=>'btn btn-danger btn-block'])!!}

                                        {!!Form::close()!!}

                                    </div>
                                </div>
                            </div>
                        @endauth
                    </aside>
                </div>
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <!--Services Single-->
                    <div class="service-single">
                        <div class="inner-box">
                            <div class="image">
                                <h3 class="text-center">{{$lajsni->title}}</h3>
                                <br>
                                <img src="{{asset('uploads/lajsni/'.$lajsni->image)}}" alt=""
                                     style="height: 400px;width: 400px; object-fit: cover; margin:auto"/>
                            </div>
                            <div class="lower-content">
                                <h3>Детални Информации</h3>
                                <div class="text">
                                    <p>{{$lajsni->body}}</p>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
@endsection

