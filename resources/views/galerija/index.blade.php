@extends ('NewDesign.template')
@section('title','Galerija')
@section('_pagetitle')
    <section class="page-title" style="background-image:url(images/title/2.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-8 col-xs-12">
                    <h1>Галерија</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Почетна</a></li>
                        <li class="active">Галерија</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('create-content')


    <div class="container-fluid padding">
        <div class="row padding">
            <div class="col-md-10 text-center">


            </div>
            @auth
                <div class="col-md-2">

                    <a href="{{route("galerija.create")}}" class="btn btn-info">Додај нова Слика</a>
                </div>
            @endauth
        </div>
    </div>


    <!--Project Section Two-->
    <section class="">
        <br>
        <br>
        <!--Porfolio Tabs-->

        <!--Tabs Content-->
        <div class="p-tabs-content">
            <!--Portfolio Tab / Active Tab-->
            <div class="p-tab active-tab" id="p-tab-1">
                <div class="project-carousel owl-theme owl-carousel">

                @foreach($galerija as $galerijas)
                    <!--Gallery Block-->
                        <!--Gallery Block-->
                        <div class="gallery-block">
                            <div class="inner-box">
                                <div class="image">
                                    <img src="{{asset('uploads/galerija/'.$galerijas->image)}}" alt=""
                                         style="height:400px;object-fit: cover"/>
                                    <div class="overlay-box">
                                        <div class="content clearfix">
                                            <div class="pull-left">
                                                <a href="{{asset('uploads/galerija/'.$galerijas->image)}}"
                                                   class="plus-icon lightbox-image"
                                                   data-fancybox-group="gallery-one"><span class="category big-text"
                                                        ><H3>{{$galerijas->title}}</H3></span></a>
                                            </div>
                                            <div class="pull-right">
                                                <a href="{{asset('uploads/galerija/'.$galerijas->image)}}"
                                                   class="plus-icon lightbox-image"
                                                   data-fancybox-group="gallery-one"><span
                                                        class="icon fa fa-plus"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection

