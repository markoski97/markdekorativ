@section('_mail')
<!--Help Section-->
<section class="help-section" style="background-image:url(images/background/2.jpg);">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Content Column-->
            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <h2>Контакт</h2>
                    <div class="text">За сите прашања или коментари можете да не <br>
                        контактирате преку формуларот во продолжение.</div>
                </div>
            </div>
            <!--Form Column-->
            <div class="form-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner-column">

                    <!-- Default Form -->
                    <div class="default-form">
                        <!--Contact Form-->
                        <form method="post" action="contact.html">
                            <div class="row clearfix">
                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input type="text" name="username" placeholder="Име и Презиме" required>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 form-group">
                                    <input type="email" name="email" placeholder="Емаил" required>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <input type="text" name="subject" placeholder="Прашање" required>
                                </div>

                                <div class="col-md-12 col-sm-12 col-xs-12 form-group">
                                    <input type="text" name="subject" placeholder="Телефон" required>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <textarea name="message" placeholder="Порака"></textarea>
                                </div>

                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group">
                                    <button class="theme-btn btn-style-one" type="submit" name="submit-form">Испрати</button>
                                </div>

                            </div>
                        </form>

                    </div>
                    <!--End Default Form -->

                </div>
            </div>
        </div>
    </div>
</section>
<!--End Help Section-->
@endsection
