@section('_contact')
<!--Contact Section-->
<section class="contact-page-section">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Form Column-->
            <div class="column form-column col-md-7 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <h2>Имаш прашање? Пиши Ни</h2>
                    <!--Contact Form-->
                    <div class="contact-form">
                        <form action="{{ url('contact') }}" method="POST">
                            {{csrf_field()}}

                            <div class="row clearfix">
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="imeprezime" id="imeprezime"value="" placeholder="Име и презиме" required class="form-control">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" name="email" id="email "value="" placeholder="Email" required class="form-control">
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" name="number" id="number" value="" placeholder="Телефон" required class="form-control">
                                </div>

                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" name="subject" id="subject" value="" placeholder="Прашање" required class="form-control">
                                </div>

                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <textarea name="message" id="message" placeholder="Message" class="form-control"></textarea>
                                </div>

                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="theme-btn btn-style-one">Испрати</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--Info Column-->
            <div class="column info-column col-md-5 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <h2>Контакт</h2>
                    <div class="information-blocks">
                        <div class="text">Најсовремен и реномиран изложбен салон за Ламинати,Внатрешни и Надворешни врати во центарот на Прилеп.
                        Бесплатен паркинг за сите наши клиенти.
                        </div>
                        <!--Contact Info-->
                        <div class="contact-info">
                            <div class="inner">
                                <div class="icon-box">
                                    <span class="flaticon-maps-and-flags"></span>
                                </div>
                                <h3>Адреса</h3>
                                <div class="text">Улица "Егејска" бр.75 Прилеп,Маkедонија,7500</div>
                            </div>
                        </div>

                        <!--Contact Info-->
                        <div class="contact-info">
                            <div class="inner">
                                <div class="icon-box">
                                    <span class="flaticon-web"></span>
                                </div>
                                <h3>Email</h3>
                                <div class="text">gigantmk1@gmail.com</div>
                            </div>
                        </div>
                        <!--Contact Info-->
                        <div class="contact-info">
                            <div class="inner">
                                <div class="icon-box">
                                    <span class="flaticon-technology"></span>
                                </div>
                                <h3>Телефон</h3>
                                <div class="text">(00389) 75 222 268</div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Contact Section-->
    @endsection
