@section('_akcii')
    @if($akcii->count())
    <!--Services Section Two-->
    <section class="services-section-two">
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title centered">
                <h2>Акции</h2>
                <div class="text">Производи кои се на акција и се на ограничена количина.</div>
            </div>

            <div class="row clearfix">
            @foreach($akcii as $akcija)
                <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12">
                        <div class="inner-box">
                            <div class="image">
                                <a href="{{url('akcii/'.$akcija->id)}}"><img src="{{asset('uploads/akcii/'.$akcija->image)}}"
                                                                             alt=""/></a>
                            </div>
                            <div class="lower-box">
                                <div class="content">
                                    <a href="{{url('akcii/'.$akcija->id)}}" class="arrow-box"><span
                                            class="fa fa-angle-right"></span></a>
                                    <h3><a href="{{url('akcii/'.$akcija->id)}}">{{$akcija->title}}</a></h3>
                                    <div class="text">{{$akcija->price}} денари</div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="text-center">
                <a href="{{url('akcii')}}" class="theme-btn btn-style-one">Види ги Сите</a>
            </div>
        </div>
    </section>
@endif
@endsection
