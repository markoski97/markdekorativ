<!--Footer Section Two-->
<section class="footer-style-two">
    <div class="auto-container">
        <div class="widgets-section">
            <div class="row clearfix">
                <!--Footer Column-->
                <div class="row text-center">
                    <div class="col-md-4">
                        <a href="{{route('login')}}"> <img src="{{asset('img/1blacksvg.svg')}}" style="height:50px; width:300px; "></a>
                        <br>
                        <hr class="light">
                        <p>(00389) 75 222 268</p>
                        <p>gigantmk1@gmail.com</p>
                        <p>улица "Егејска" бр.75</p>
                        <p>Прилеп, Маkедонија,7500</p>
                    </div>
                    <div class="col-md-4">
                        <hr class="light">
                        <h5>Работно Време</h5>
                        <hr class="light">
                        <p>Понеделник-Петок:08:00-20:00 часот</p>
                        <p>Среда:08:00-16:00 часот</p>
                        <p>Сабота:08:00-16:00 часот</p>
                        <p>Недела и Државни празници:Неработен</p>
                    </div>
                    <div class="col-md-4">
                        <hr class="light">
                        <h5>Корисни информации</h5>
                        <hr class="light">
                        <ul class="list-unstyled">
                            <li><a href="{{url('proizvodi')}}">Производи</a></li>
                            <li><a href="{{url('akcii')}}">Акции</a></li>
                            <li><a href="{{url('galerija')}}">Галерија</a></li>
                            <li><a href="{{url('informacii')}}">Информации</a></li>
                            <li><a href="{{url('contact')}}">Контакт</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="copyright">&copy;| 2019| Mark Dekorativ | Markoski Petar </div>
    </div>
</section>
<!--End Footer Section Two-->


