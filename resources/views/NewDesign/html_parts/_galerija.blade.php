@section('_galerija')
    <!--Project Section Two-->
    <section class="">

        <!--Porfolio Tabs-->
        <div class="project-tab">
            <div class="auto-container">
                <!--Sec Title-->
                <div class="sec-title centered">
                    <h2>Галерија</h2>
                </div>
                <div class="clearfix">
                    <div>
                        <div class="more-projects centered"><a href={{url('galerija')}}>Видија цела Галерија</a></div>
                    </div>
                </div>
                <br>

                <!--Tabs Content-->
                <div class="p-tabs-content">
                    <!--Portfolio Tab / Active Tab-->
                    <div class="p-tab active-tab" id="p-tab-1">
                        <div class="project-carousel owl-theme owl-carousel">

                            <!--Gallery Block-->
                            <div class="gallery-block">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="images/galerijafront/1.jpg" alt="" style="height:400px;object-fit: cover"/>
                                        <div class="overlay-box">
                                            <div class="content clearfix">
                                                    <a href="images/galerijafront/1.jpg" class="plus-icon lightbox-image"
                                                       data-fancybox-group="gallery-one"><span
                                                            class="icon fa fa-plus"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="gallery-block">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="images/galerijafront/2.jpg" alt="" style="height:400px;object-fit: cover"/>
                                        <div class="overlay-box">
                                            <div class="content clearfix">
                                                <a href="images/galerijafront/2.jpg" class="plus-icon lightbox-image"
                                                   data-fancybox-group="gallery-one"><span
                                                        class="icon fa fa-plus"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="gallery-block">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="images/galerijafront/3.jpg" alt=""style="height:400px;object-fit: cover"/>
                                        <div class="overlay-box">
                                            <div class="content clearfix">
                                                <a href="images/galerijafront/3.jpg" class="plus-icon lightbox-image"
                                                   data-fancybox-group="gallery-one"><span
                                                        class="icon fa fa-plus"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="gallery-block">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="images/galerijafront/4.jpg" alt=""style="height:400px;object-fit: cover"/>
                                        <div class="overlay-box">
                                            <div class="content clearfix">
                                                <a href="images/galerijafront/4.jpg" class="plus-icon lightbox-image"
                                                   data-fancybox-group="gallery-one"><span
                                                        class="icon fa fa-plus"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="gallery-block">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="images/galerijafront/5.jpg" alt=""style="height:400px;object-fit: cover"/>
                                        <div class="overlay-box">
                                            <div class="content clearfix">
                                                <a href="images/galerijafront/5.jpg" class="plus-icon lightbox-image"
                                                   data-fancybox-group="gallery-one"><span
                                                        class="icon fa fa-plus"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="gallery-block">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="images/galerijafront/6.jpg" alt=""style="height:400px;object-fit: cover"/>
                                        <div class="overlay-box">
                                            <div class="content clearfix">
                                                <a href="images/galerijafront/6.jpg" class="plus-icon lightbox-image"
                                                   data-fancybox-group="gallery-one"><span
                                                        class="icon fa fa-plus"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="gallery-block">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="images/galerijafront/7.jpg" alt=""style="height:400px;object-fit: cover"/>
                                        <div class="overlay-box">
                                            <div class="content clearfix">
                                                <a href="images/galerijafront/7.jpg" class="plus-icon lightbox-image"
                                                   data-fancybox-group="gallery-one"><span
                                                        class="icon fa fa-plus"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="gallery-block">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="images/galerijafront/8.jpg" alt=""style="height:400px;object-fit: cover"/>
                                        <div class="overlay-box">
                                            <div class="content clearfix">
                                                <a href="images/galerijafront/8.jpg" class="plus-icon lightbox-image"
                                                   data-fancybox-group="gallery-one"><span
                                                        class="icon fa fa-plus"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="gallery-block">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="images/galerijafront/9.jpg" alt=""style="height:400px;object-fit: cover"/>
                                        <div class="overlay-box">
                                            <div class="content clearfix">
                                                <a href="images/galerijafront/9.jpg" class="plus-icon lightbox-image"
                                                   data-fancybox-group="gallery-one"><span
                                                        class="icon fa fa-plus"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="gallery-block">
                                <div class="inner-box">
                                    <div class="image">
                                        <img src="images/galerijafront/10.jpg" alt=""style="height:400px;object-fit: cover"/>
                                        <div class="overlay-box">
                                            <div class="content clearfix">
                                                <a href="images/galerijafront/10.jpg" class="plus-icon lightbox-image"
                                                   data-fancybox-group="gallery-one"><span
                                                        class="icon fa fa-plus"></span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <br><br>
    </section>
    <!--End Project Section Two-->
@endsection
