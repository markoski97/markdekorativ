@section('_kontakt')
<section class="call-to-action-section" style="background-image:url(images/background/image1.jfif);">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Content Column-->
            <div class="content-column col-md-8 col-sm-12 col-xs-12">
                <div class="sec-title light">
                    <h2>Им служиме на нашите клиенти повеќе од 15 години</h2>
                </div>
                <div class="text">Во Прилеп постоиме од 2004 година со гредежното стовариште Гигант, од 2015 година и со изложбениот салон МаркДекоратив. </div>
            </div>
            <!--Btn Column-->
            <div class="btn-column col-md-4 col-sm-12 col-xs-12">
                <a href="{{asset('contact')}}" class="theme-btn btn-style-one">Контакт</a>
            </div>
        </div>
    </div>
</section>

    @endsection
