@section('_sorabotka')
<!--Welcome Section Two-->
<section class="welcome-section-two">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Content Column-->
            <div class="content-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <!--Sec Title-->
                    <div class="sec-title">
                        <h2>МаркДекоратив</h2>
                    </div>
                    <div class="big-text">Во рамките на нашата заложба е да им обезбедиме на нашите клиенти место каде ќе можат да најдат секаков вид на ламинати, внатрешни врати, надворешни врати, и сите други акцесоари кои им се потребни при уредување на нивниот дом.
                        Вршиме бесплатна испорака до вашиот дом, во Прилеп и околината.</div>
                    <div class="big-text">Вршиме бесплатна испорака до вашиот дом,Во прилеп и околината.</div>
                    <a href="{{asset('contact')}}" class="theme-btn btn-style-one">Контакт</a>
                </div>
            </div>
            <!--Form Column-->
            <div class="form-column col-md-6 col-sm-12 col-xs-12">
                <div class="inner-column">
                    <div class="title-box">
                        <h3 class="text-center">Побарај понуда <div class="text">(важи само за фирми и правни лица)</div></h3>

                    </div>
                    <div class="register-form">
                        <form action="{{ url('sorabotka') }}" method="POST">
                            {{csrf_field()}}

                            <div class="row clearfix">
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="imefirma" id="imefirma"value="" placeholder="Име на Фирма" required class="form-control">
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" name="email" id="email "value="" placeholder="Email" required class="form-control">
                                </div>
                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <input type="text" name="number" id="number" value="" placeholder="Телефон" required class="form-control">
                                </div>

                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <textarea name="message" id="message" placeholder="Message" class="form-control"></textarea>
                                </div>

                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <label>Прикачи дукумент:</label>
                                    <input type="file" name="file" accept="file_extension|image/*|media_type">
                                </div>

                                <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                    <button type="submit" class="theme-btn btn-style-one">Испрати</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                   {{-- <div class="register-form">
                        <div class="icon-box">
                            <span class="icon flaticon-send"></span>
                        </div>
                        <!-- Register Form -->
                        <div class="rejister-form">

                            <!--Contact Form-->
                            <form method="post" action="contact.html">
                                <div class="form-group">
                                    <input type="text" name="name" placeholder="Име на Фирма" required>
                                </div>

                                <div class="form-group">
                                    <input type="email" name="email" placeholder="Емаил" required>
                                </div>

                                <div class="form-group">
                                    <input type="email" name="phone" placeholder="Телефон" required>
                                </div>

                                <div class="form-group">
                                    <textarea name="message" placeholder="Порака"></textarea>
                                </div>

                                <div class="form-group">
                                    <label>Прикачи дукумент:</label>
                                    <input type="file" name="files[]" accept="file_extension|image/*|media_type" multiple>
                                </div>

                                <div class="form-group">
                                    <button class="theme-btn btn-style-one" type="submit" name="submit-form">Испрати</button>
                                </div>
                            </form>
                        </div>--}}
                        <!--End Register Form -->

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End Welcome Section Two-->
    @endsection
