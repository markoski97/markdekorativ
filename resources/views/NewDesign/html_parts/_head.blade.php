<head>
    <meta charset="utf-8">
    <!-- Stylesheets -->

    <link rel="stylesheet" type="text/css" href="{{ url('/css/bootstrap.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('/css/style.css') }}" />

    <link rel="stylesheet" type="text/css" href="{{ url('/plugins/revolution/css/settings.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('/plugins/revolution/css/layers.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ url('/plugins/revolution/css/navigation.css') }}" />


    <link rel="stylesheet" type="text/css" href="{{ url('/css/responsive.css') }}" />
    <!--Color Themes-->
    <link rel="stylesheet" type="text/css" href="{{ url('/css/red-theme.css') }}" />
    <!-- Responsive -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!--[if lt IE 9]><script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script><![endif]-->

    <title>MarkDekorativ|@yield('title')</title>
    <link rel="icon" href="{{asset('images/kec.png')}}" type="image/x-icon">
</head>
