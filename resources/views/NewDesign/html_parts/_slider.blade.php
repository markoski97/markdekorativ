
@section('_slider')
<!--Main Slider-->
<section class="main-slider">

    <div class="rev_slider_wrapper fullwidthbanner-container"  id="rev_slider_one_wrapper" data-source="gallery">
        <div class="rev_slider fullwidthabanner" id="rev_slider_one" data-version="5.4.1">
            <ul>

                <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1688" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-2.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/1113.jpg">

                    <div class="tp-caption"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingtop="[0,0,0,0]"
                         data-responsive_offset="on"
                         data-type="text"
                         data-height="none"
                         data-width="['700','700','700','450']"
                         data-whitespace="normal"
                         data-hoffset="['15','15','15','15']"
                         data-voffset="['-150','-140','-130','-120']"
                         data-x="['left','left','left','left']"
                         data-y="['middle','middle','middle','middle']"
                         data-textalign="['top','top','top','top']"
                         data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                         style="z-index: 7; white-space: nowrap;text-transform:left;">
                        <div class="large-text">Комерцијален и Станбен под
                        </div>
                    </div>

                    <div class="tp-caption"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingtop="[0,0,0,0]"
                         data-responsive_offset="on"
                         data-type="text"
                         data-height="none"
                         data-width="['900','800','800','450']"
                         data-whitespace="normal"
                         data-hoffset="['15','15','15','15']"
                         data-voffset="['-50','-50','-50','-50']"
                         data-x="['left','left','left','left']"
                         data-y="['middle','middle','middle','middle']"
                         data-textalign="['top','top','top','top']"
                         data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                         style="z-index: 7; white-space: nowrap;text-transform:left;">
                        <h1>Mарк Декоратив </h1>
                    </div>

                    <div class="tp-caption"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingtop="[0,0,0,0]"
                         data-responsive_offset="on"
                         data-type="text"
                         data-height="none"
                         data-width="['600','700','700','450']"
                         data-whitespace="normal"
                         data-hoffset="['15','15','15','15']"
                         data-voffset="['60','60','60','40']"
                         data-x="['left','left','left','left']"
                         data-y="['middle','middle','middle','middle']"
                         data-textalign="['top','top','top','top']"
                         data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                         style="z-index: 7; white-space: nowrap;text-transform:left;">
                        <div class="text">Широк асортимант на ламинати,внатрешни и надворешни врати.</div>
                    </div>

                    <div class="tp-caption tp-resizeme"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingtop="[0,0,0,0]"
                         data-responsive_offset="on"
                         data-type="text"
                         data-height="none"
                         data-width="['700','700','700','450']"
                         data-whitespace="normal"
                         data-hoffset="['15','15','15','15']"
                         data-voffset="['160','135','155','120']"
                         data-x="['left','left','left','left']"
                         data-y="['middle','middle','middle','middle']"
                         data-textalign="['top','top','top','top']"
                         data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                         style="z-index: 7; white-space: nowrap;text-transform:left;">
                        <div class="btns-box">
                            <a href="{{url('proizvodi')}}" class="theme-btn btn-style-one">Производи</a> &ensp; &ensp; <a href="{{url('galerija')}}" class="theme-btn btn-style-two">Галерија</a>
                        </div>
                    </div>

                </li>

                <li data-description="Slide Description" data-easein="default" data-easeout="default" data-fsmasterspeed="1500" data-fsslotamount="7" data-fstransition="fade" data-hideafterloop="0" data-hideslideonmobile="off" data-index="rs-1687" data-masterspeed="default" data-param1="" data-param10="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-rotate="0" data-saveperformance="off" data-slotamount="default" data-thumb="images/main-slider/image-1.jpg" data-title="Slide Title" data-transition="parallaxvertical">
                    <img alt="" class="rev-slidebg" data-bgfit="cover" data-bgparallax="10" data-bgposition="center center" data-bgrepeat="no-repeat" data-no-retina="" src="images/main-slider/mal.jpg">

                    <div class="tp-caption"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingtop="[0,0,0,0]"
                         data-responsive_offset="on"
                         data-type="text"
                         data-height="none"
                         data-width="['700','700','700','450']"
                         data-whitespace="normal"
                         data-hoffset="['15','15','15','15']"
                         data-voffset="['-150','-120','-120','-120']"
                         data-x="['left','left','left','left']"
                         data-y="['middle','middle','middle','middle']"
                         data-textalign="['top','top','top','top']"
                         data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                         style="z-index: 7; white-space: nowrap;text-transform:left;">
                        <div class="large-text"></div>


                    </div>

                    <div class="tp-caption"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingtop="[0,0,0,0]"
                         data-responsive_offset="on"
                         data-type="text"
                         data-height="none"
                         data-width="['900','800','800','450']
                         data-whitespace="normal"
                         data-hoffset="['15','15','15','15']"
                         data-voffset="['-50','-40','-40','-50']"
                         data-x="['left','left','left','left']"
                         data-y="['middle','middle','middle','middle']"
                         data-textalign="['top','top','top','top']"
                         data-frames='[{"from":"y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                         style="z-index: 7; white-space: nowrap;text-transform:left;">"
                        <h1>Акции</h1>
                    </div>

                    <div class="tp-caption"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingtop="[0,0,0,0]"
                         data-responsive_offset="on"
                         data-type="text"
                         data-height="none"
                         data-width="['600','600','700','450']"
                         data-whitespace="normal"
                         data-hoffset="['15','15','15','15']"
                         data-voffset="['60','50','50','40']"
                         data-x="['left','left','left','left']"
                         data-y="['middle','middle','middle','middle']"
                         data-textalign="['top','top','top','top']"
                         data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                         style="z-index: 7; white-space: nowrap;text-transform:left;">
                        <div class="text">Производи кои се на на акција до одреден период и одредена количина.</div>
                    </div>

                    <div class="tp-caption tp-resizeme"
                         data-paddingbottom="[0,0,0,0]"
                         data-paddingleft="[0,0,0,0]"
                         data-paddingright="[0,0,0,0]"
                         data-paddingtop="[0,0,0,0]"
                         data-responsive_offset="on"
                         data-type="text"
                         data-height="none"
                         data-width="['700','700','700','450']"
                         data-whitespace="normal"
                         data-hoffset="['15','15','15','15']"
                         data-voffset="['160','125','125','120']"
                         data-x="['left','left','left','left']"
                         data-y="['middle','middle','middle','middle']"
                         data-textalign="['top','top','top','top']"
                         data-frames='[{"from":"y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;","mask":"x:0px;y:0px;s:inherit;e:inherit;","speed":1500,"to":"o:1;","delay":1000,"ease":"Power3.easeInOut"},{"delay":"wait","speed":1000,"to":"auto:auto;","mask":"x:0;y:0;s:inherit;e:inherit;","ease":"Power3.easeInOut"}]'
                         style="z-index: 7; white-space: nowrap;text-transform:left;">
                        <div class="btns-box">
                            <a href="{{url('akcii')}}" class="theme-btn btn-style-one">Акции</a> &ensp; &ensp;
                        </div>
                    </div>

                </li>
            </ul>
        </div>
    </div>
</section>
<!--End Main Slider-->

    @endsection
