@section('_map')
    <!--Map Section-->
    <section class="map-section">
        <div class="mapouter">
            <div class="gmap_canvas">
                <iframe width="100%" height="520" id="gmap_canvas"
                        src="https://maps.google.com/maps?q=MarkDekorativ&t=&z=15&ie=UTF8&iwloc=&output=embed"
                        frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                <a href="https://www.embedgooglemap.net">embedgooglemap.net</a></div>
            <style>.mapouter {
                    position: relative;
                    text-align: right;
                    height: 520px;
                    width: 100%;
                }

                .gmap_canvas {
                    overflow: hidden;
                    background: none !important;
                    height: 520px;
                    width: 100%;
                }</style>
        </div>
        <script src="http://maps.google.com/maps/api/js?key=AIzaSyBKS14AnP3HCIVlUpPKtGp7CbYuMtcXE2o"></script>

        <script type="text/javascript" src="{{URL::asset('/js/map-script.js')}}"></script>
    </section>
@endsection

