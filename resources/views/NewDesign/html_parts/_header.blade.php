<div class="page-wrapper">
    <!-- Preloader -->
    <div class="preloader"></div>

    <!-- Main Header / Header Style Two-->
    <header class="main-header header-style-two">
        <!-- Main Box -->
        <div class="main-box">
            <div class="auto-container">
                <div class="outer-container clearfix">
                 <!--Logo Box-->
              <div class="logo-box">
                     <div class="logo"><a href="{{url('/')}}"><img src="{{asset('images/logo-malo.jpg')}}" alt=""></a></div>
                 </div>

                <!--Nav Outer-->
                    <div class="nav-outer clearfix">
                        <!-- Main Menu -->
                        <nav class="main-menu">
                            <div class="navbar-header">
                                <!-- Toggle Button -->
                                <button type="button" class="navbar-toggle" data-toggle="collapse"
                                        data-target=".navbar-collapse">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <div class="navbar-collapse collapse clearfix">
                                <ul class="navigation clearfix">
                                    <li><a href="{{url('/')}}">Почетна</a></li>
                                    <li class="dropdown"><a href="{{url('proizvodi')}}">Производи</a>
                                        <ul>
                                            <li><a href="{{url('laminat')}}">Ламинат</a></li>
                                            <li><a href="{{url('lajsni')}}">Лајсни</a></li>
                                            <li><a href="{{url('stakleni')}}">Стаклени Блокови</a></li>
                                            <li><a href="{{url('pdf/1')}}">Внатрешни Врати</a></li>
                                            <li><a href="{{url('pdf/2')}}">Надворешни Врати</a></li>
                                            <li><a href="{{url('ostanato')}}">Останато</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="{{url('akcii')}}">Акции</a></li>
                                    <li><a href="{{url('informacii')}}">Информации</a></li>
                                    <li><a href="{{url('galerija')}}">Галерија</a></li>
                                    <li><a href="{{url('contact')}}">Контакт</a></li>
                                </ul>
                            </div>
                        </nav>
                        <!-- Main Menu End-->

                    {{-- <!--Search Box-->
                     <div class="search-box-outer">
                         <div class="dropdown">
                             <button class="search-box-btn dropdown-toggle" type="button" id="dropdownMenu3" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fa fa-search"></span></button>
                             <ul class="dropdown-menu pull-right search-panel" aria-labelledby="dropdownMenu3">
                                 <li class="panel-outer">
                                     <div class="form-container">
                                         <form method="post" action="blog.html">
                                             <div class="form-group">
                                                 <input type="search" name="field-name" value="" placeholder="Search Here" required>
                                                 <button type="submit" class="search-btn"><span class="fa fa-search"></span></button>
                                             </div>
                                         </form>
                                     </div>
                                 </li>
                             </ul>
                         </div>
                     </div>

                 </div>--}}
                    <!--Nav Outer End-->

                    </div>
                </div>
            </div>

        </div>
    </header>

</div>
