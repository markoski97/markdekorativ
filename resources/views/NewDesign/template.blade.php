<!DOCTYPE html>
<html>
@include('NewDesign.html_parts._head')
<body>
@include('NewDesign.html_parts._header')
@include('NewDesign.html_parts._messages')
@yield('_pagetitle')
@yield('_map')
@include('NewDesign.html_parts._messages')
@yield('_contact')
@yield('_slider')
@yield('_sorabotka')
@yield('_kontakt')
@yield('_acordion')

@yield ('_klasinaotpornost')
@yield('create-content')
@yield('_akcii')
@yield('_mail')

@yield('_galerija')
@include('NewDesign.html_parts._footer')
@include('NewDesign.html_parts._scripts')

</body>


