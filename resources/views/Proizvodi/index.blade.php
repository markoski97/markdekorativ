@extends ('NewDesign.template')
@section('title','Proizvodi-index')
@section('_pagetitle')
    <section class="page-title" style="background-image:url(images/title/2.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-8 col-xs-12">
                    <h1>Производи</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Почетна</a></li>
                        <li class="active">Производи</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('create-content')

    <div class="container-fluid padding">
        <div class="row padding">
            <div class="col-md-10">

            </div>
            @auth
                <div class="col-md-2">

                    <a href="{{route("proizvodi.create")}}" class="btn btn-info">Креирај Нов Производ</a>
                </div>
            @endauth
        </div>
    </div>
    <section class="services-section-two">
        <div class="auto-container">
            <!--Sec Title-->
            <div class="sec-title centered">
                <h2>Производи</h2>
            </div>
            <div class="row clearfix">
            @foreach($proizvodi as $proizvodis)
                <!--Services Block-->
                    <div class="services-block col-md-4 col-sm-6 col-xs-12" style="margin-top: 25px;margin-bottom: 25px">
                        <div class="inner-box">
                            <div class="image">
                                <a href="{{url('ostanato/'.$proizvodis->id)}}"><img
                                        src="{{asset('uploads/proizvodi/'.$proizvodis->image)}}" style="height: 400px;width: 400px; object-fit: cover"
                                        alt=""/></a>
                            </div>
                            <div class="lower-box">
                                <div class="content">
                                    <a href="{{$proizvodis->link()}}" class="arrow-box"><span
                                            class="fa fa-angle-right"></span></a>
                                    <div class="text"><h2>{{$proizvodis->title}}</h2></div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection



