@extends('NewDesign.template')

@section('title','Akcii-create')

    @section('create-content')

        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h1>Креирај Нова Акција</h1>
                <hr>
                <form method="POST" enctype="multipart/form-data" action="{{ route('akcii.store') }}">
                    @csrf

                    <div class="form-group">
                        <label name="title">Наслов:</label>
                        <input id="title" type="text" name="title"
                               class="form-control{{$errors->has('title') ? 'is-invalid' : ''}}"
                               value="{{ old('title') }}"
                               autocomplete="title" autofocus required>
                        @if($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('title') }}</strong>
                            </span>

                        @endif
                    </div>


                    <div class="form-group">
                        <label name="price">Цена:</label>
                        <input id="price" type="text" name="price"
                               class="form-control{{$errors->has('price') ? 'is-invalid' : ''}}"
                               value="{{ old('price') }}"
                               autocomplete="price" autofocus required>
                        @if($errors->has('price'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('price') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="form-group">
                        <label name="title">Дополнителни инфромации:</label>
                        <textarea id="body" type="text" name="body"
                               class="form-control{{$errors->has('body') ? 'is-invalid' : ''}}"
                               value="{{ old('body') }}"
                                  autocomplete="body" autofocus required> </textarea>
                        @if($errors->has('body'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('body') }}</strong>
                            </span>

                        @endif
                    </div>

                    <div class="row">
                        <label for="image" class="col-md-4 col-form-label" >Слика:</label>
                        <input type="file" class="form-control-file" id="image" name="image" required>
                        @if($errors->has('image'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{$errors->first('image') }}</strong>
                            </span>
                        @endif
                    </div>

                    <input type="submit" value="Креирај Акција" class="btn btn-success btn-lg btn-block">
                    <br>
                    <br>
                    <br>
                </form>
            </div>
        </div>


    @endsection
