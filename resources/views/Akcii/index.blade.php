@extends ('NewDesign.template')
@section('title','Akcii')

@section('create-content')

    <section class="page-title" style="background-image:url(images/title/2.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-8 col-xs-12">
                    <h1>Акции</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Почетна</a></li>
                        <li class="active">Акции</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <br>
    <br>

    <div class="container-fluid padding">
        @auth
            <div class="col-md-2 offset-10">
                <a href="{{route("akcii.create")}}" class="btn btn-info">Креирај нова акција</a>
            </div>
        @endauth
    </div>

    <section class="">
        <div class="auto-container">
            <!--Sec Title-->
            <div class="row clearfix">
            @if($akcii->count())
                    <div class="container-fluid padding">
                        <div class="col-md-12" id="akcii">
                            <h2 class="text-center">Во продолжение можете да ги видите сите производи кои се на попуст/акција.</h2>
                        </div>
                    </div>
                    <br>
                    <br>
                @foreach($akcii as $akcija)
                    <!--Services Block-->
                        <div class="services-block col-md-4 col-sm-6 col-xs-12" style="margin-top: 25px;margin-bottom: 25px">
                            <div class="inner-box">
                                <div class="image">
                                    <a href="{{url('akcii/'.$akcija->id)}}"><img
                                            src="{{asset('uploads/akcii/'.$akcija->image)}}" style="height: 400px;width: 400px; object-fit: cover"
                                            alt=""/></a>
                                </div>
                                <div class="lower-box">
                                    <div class="content">
                                        <a href="{{url('akcii/'.$akcija->id)}}" class="arrow-box"><span
                                                class="fa fa-angle-right"></span></a>
                                        <h3><a href="{{url('akcii/'.$akcija->id)}}">{{$akcija->title}}</a></h3>
                                        <div class="text">{{$akcija->price}} денари</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
            </div>
        </div>
        @else
            <div class="container-fluid padding">
                <div class="col-md-12">
                    <h1 class="text-center">Во Моментов Нема Производи На Акција.</h1>
                </div>
        @endif
    </section>

    <div class="text-center">
        {!! $akcii->links()!!}
    </div>
<br>
<br>

@endsection

