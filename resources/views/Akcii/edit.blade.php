@extends ('NewDesign.template')

@section('title','Akcii-edit')

@section('create-content')
    <section class="page-title" style="background-image:url(images/title/2.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-8 col-xs-12">
                    <h1>Акции</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Почетна</a></li>
                        <li class="active">Акции</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <div class="container-fluid padding">
        <div class="row padding">
            <div class="col-md-12">
                {!! Form::model($akcii, ['route' => ['akcii.update', $akcii->id],'method'=>'PUT','enctype'=>"multipart/form-data" ]) !!}
                <div class="container-fluid padding">
                    <div class="row padding">
                        <div class="col-md-8">
                        {{ Form::label('title', 'Наслов:') }}
                        {{ Form::text('title', null, ["class" => 'form-control']) }}


                        {{ Form::label('price', 'Цена:') }}
                        {{ Form::text('price', null, ["class" => 'form-control']) }}

                        {{ Form::label('body', 'Детални информации') }}
                        {{ Form::textarea('body', null, ["class" => 'form-control']) }}

                        {{ Form::label('image', 'Слика') }}
                        {{ Form::file('image', null, ["class" => 'form-control']) }}


                    </div>
                    <div class="col-md-4">
                        <div class="well">
                            <dl class="dl-horizontal">
                                <dt>Креирано на:</dt>
                                <dd>{{date('M j, Y H:i',strtotime($akcii->created_at))}}</dd>
                            </dl>

                            <dl class="dl-horizontal">
                                <dt>Последна промена на:</dt>
                                <dd>{{date('M j, Y H:i',strtotime($akcii->updated_at))}}</dd>
                            </dl>
                            <hr>

                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="{{route('akcii.show',$akcii->id)}}"
                                       class="btn btn-danger btn-block">Откажи</a>

                                </div>

                                <div class="col-sm-6">
                                    {{Form::submit('Зачувајги промените',['class'=>'btn btn-success btn-block',])}}
                                </div>
                            </div>


                        </div>


                    </div>
                    {!! Form::close()!!}
                </div>
            </div>






@endsection
