@extends ('NewDesign.template')

@section('title','Akcii-show')
@section('_pagetitle')
    <section class="page-title" style="background-image:url(../images/title/2.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-8 col-xs-12">
                    <h1>{{$akcii->title}}</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Почетна</a></li>
                        <li class="active">Акции</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('create-content')
<br>
<!--Sidebar Page Container-->
<div class="sidebar-page-container">
    <div class="auto-container">
        <div class="row clearfix">
            <!--Sidebar Side-->
            <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">

                <aside class="sidebar default-sidebar with-padding">

                    <!-- Sidebar Category -->
                    <div class="sidebar-widget sidebar-category">
                        <ul class="list">
                            <li><a href="{{url('/')}}">Почетна</a></li>
                            <li><a href="{{url('/akcii')}}">Акции</a></li>
                            <li><a href="{{url('/laminat')}}">Ламинат</a></li>
                            <li><a href="{{url('/pdf/1')}}">Внатрешни Врати</a></li>
                            <li><a href="{{url('/pdf/2')}}">Надворешни Врати</a></li>
                            <li><a href="{{url('/stakleni')}}">Стаклени Блокови</a></li>
                            <li><a href="{{url('/galerija')}}">Галерија</a></li>
                            <li><a href="{{url('/informacii')}}">Инфромации</a></li>
                            <li><a href="{{url('/kontakt')}}">Контакт</a></li>
                        </ul>
                    </div>

                    @auth
                            <div class="well">
                                <dl class="dl-horizontal">

                                    <dt>Креирано на:</dt>
                                    <dd>{{date('M j, Y H:i',strtotime($akcii->created_at))}}</dd>
                                </dl>

                                <dl class="dl-horizontal">
                                    <dt>Последна промена на:</dt>
                                    <dd>{{date('M j, Y H:i',strtotime($akcii->updated_at))}}</dd>
                                </dl>
                                <hr>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="{{route('akcii.edit',$akcii->id)}}" class="btn btn-primary btn-block">Промени</a>

                                    </div>

                                    <div class="col-sm-6">

                                        {!!Form::open(['route'=>['akcii.destroy',$akcii->id],'method'=>'DELETE','enctype'=>"multipart/form-data"])!!}

                                        {!!Form::submit('Избриши',['class'=>'btn btn-danger btn-block'])!!}

                                        {!!Form::close()!!}

                                    </div>
                                </div>
                            </div>
                    @endauth


                </aside>
            </div>
            <!--Content Side-->
            <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <!--Services Single-->
                <div class="service-single">
                    <div class="inner-box">
                        <div class="image">
                            <h3 class="text-center">{{$akcii->title}}</h3>
                            <img src="{{asset('uploads/akcii/'.$akcii->image)}}" alt="" style="height: 400px;width: 700px; object-fit: cover"/>
                        </div>
                        <div class="lower-content">
                            <h3>Детални Информации</h3>
                            <div class="text">
                                <p>{{$akcii->body}}</p>
                                <h3>Цена</h3>
                                <p>{{$akcii->price}} денари</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

    {{--<div class="container-fluid padding">
        <div class="col-md-12">
            <div class="panel panel-default">
                @if(!$akcii)
                    <div>
                        <h1>Во овој момент нема производи на акција.</h1>
                    </div>
                @else
                    <div class="panel-body">
                        <div class="col-md-4">
                            <ul class="list-group">
                                <h2 class="text-center">Додатни информации:</h2>
                                <li class="list-group-item">Име:{{ $akcii->title }}</li>
                                <li class="list-group-item">Цена:{{ $akcii->price }}</li>
                                <textarea class="lead">{{$akcii->body}}</textarea>
                            </ul>

                        </div>

                        <div class="col-md-8">
                            <div class="list-group">
                                <img src="/storage/uploads/{{ $akcii->image }}"/>
                            </div>

                        </div>
                    </div>
            </div>
        </div>
        @auth
            <div class="col-md-4">
                <div class="well">
                    <dl class="dl-horizontal">
                        <dt>Креирано на:</dt>
                        <dd>{{date('M j, Y H:i',strtotime($akcii->created_at))}}</dd>
                    </dl>

                    <dl class="dl-horizontal">
                        <dt>Последна промена на:</dt>
                        <dd>{{date('M j, Y H:i',strtotime($akcii->updated_at))}}</dd>
                    </dl>
                    <hr>

                    <div class="row">
                        <div class="col-sm-6">
                            <a href="{{route('akcii.edit',$akcii->id)}}" class="btn btn-primary btn-block">Промени</a>

                        </div>

                        <div class="col-sm-6">

                            {!!Form::open(['route'=>['akcii.destroy',$akcii->id],'method'=>'DELETE','enctype'=>"multipart/form-data"])!!}

                            {!!Form::submit('Избриши',['class'=>'btn btn-danger btn-block'])!!}

                            {!!Form::close()!!}
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
--}}

@endsection
