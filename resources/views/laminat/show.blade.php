@extends ('NewDesign.template')

@section('title','Akcii-show')
@section('_pagetitle')
    <section class="page-title" style="background-image:url(../images/title/2.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-8 col-xs-12">
                    <h1>{{$laminat->title}}</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Почетна</a></li>
                        <li class="active">Ламинат</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('create-content')
    <br>
    <!--Sidebar Page Container-->
    <div class="sidebar-page-container">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Sidebar Side-->
                <div class="sidebar-side col-lg-4 col-md-4 col-sm-12 col-xs-12">

                    <aside class="sidebar default-sidebar with-padding">

                        <!-- Sidebar Category -->
                        <div class="sidebar-widget sidebar-category">
                            <ul class="list">
                                <li><a href="{{url('/')}}">Почетна</a></li>
                                <li><a href="{{url('/akcii')}}">Акции</a></li>
                                <li><a href="{{url('/laminat')}}">Ламинат</a></li>
                                <li><a href="{{url('/pdf/1')}}">Внатрешни Врати</a></li>
                                <li><a href="{{url('/pdf/2')}}">Надворешни Врати</a></li>
                                <li><a href="{{url('/stakleni')}}">Стаклени Блокови</a></li>
                                <li><a href="{{url('/galerija')}}">Галерија</a></li>
                                <li><a href="{{url('/informacii')}}">Инфромации</a></li>
                                <li><a href="{{url('/kontakt')}}">Контакт</a></li>
                            </ul>
                        </div>

                        @auth
                            <div class="well">
                                <dl class="dl-horizontal">

                                    <dt>Креирано на:</dt>
                                    <dd>{{date('M j, Y H:i',strtotime($laminat->created_at))}}</dd>
                                </dl>

                                <dl class="dl-horizontal">
                                    <dt>Последна промена на:</dt>
                                    <dd>{{date('M j, Y H:i',strtotime($laminat->updated_at))}}</dd>
                                </dl>
                                <hr>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <a href="{{route('akcii.edit',$laminat->id)}}" class="btn btn-primary btn-block">Промени</a>

                                    </div>

                                    <div class="col-sm-6">

                                        {!!Form::open(['route'=>['laminat.destroy',$laminat->id],'method'=>'DELETE','enctype'=>"multipart/form-data"])!!}

                                        {!!Form::submit('Избриши',['class'=>'btn btn-danger btn-block'])!!}

                                        {!!Form::close()!!}

                                    </div>
                                </div>
                            </div>
                        @endauth


                    </aside>
                </div>
                <!--Content Side-->
                <div class="content-side col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <!--Services Single-->
                    <div class="service-single">
                        <div class="inner-box">
                            <div class="image">
                                <h3 class="text-center">{{$laminat->title}}</h3>
                                <img src="{{asset('uploads/laminat/'.$laminat->image)}}" alt="" style="height: 400px;width: 700px; object-fit: cover;margin:auto"/>
                            </div>
                            <div class="lower-content">
                                <h3>Детални Информации</h3>
                                <div class="text">
                                    <h2>Дебелина</h2>
                                    <p>{{$laminat->debelina}}</p>
                                    <h2>Boja</h2>
                                    <p>{{$laminat->боја}} денари</p>
                                    <h2>Класа на Отпорност</h2>
                                    <p>{{$laminat->klasanaotpornost}}</p>
                                    <h2>Систем на греење</h2>
                                    <p>{{$laminat->sistemnagreejne}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
