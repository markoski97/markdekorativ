<div class="services-block col-md-4 col-sm-6 col-xs-12" style="margin-bottom: 25px">
    <div class="inner-box">
        <div class="image">
            <a href="{{url('laminat/'.$laminats->id)}}"><img
                    src="{{asset('uploads/laminat/'.$laminats->image)}}"
                    alt=""/></a>
        </div>
        <div class="lower-box">
            <div class="content">
                <a href="{{url('laminat/'.$laminats->id)}}" class="arrow-box"><span
                        class="fa fa-angle-right"></span></a>
                <h3><a href="{{url('laminat/'.$laminats->id)}}">{{$laminats->title}}</a></h3>
            </div>
        </div>
    </div>
</div>



