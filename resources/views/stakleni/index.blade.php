@extends('NewDesign.template')

@section('title','Ostanato')
@section('_pagetitle')
    <section class="page-title" style="background-image:url(images/title/2.jpg);">
        <div class="auto-container">
            <div class="row clearfix">
                <!--Title -->
                <div class="title-column col-md-6 col-sm-8 col-xs-12">
                    <h1>Стаклени Блокови</h1>
                </div>
                <!--Bread Crumb -->
                <div class="breadcrumb-column col-md-6 col-sm-4 col-xs-12">
                    <ul class="bread-crumb clearfix">
                        <li><a href="{{url('/')}}">Почетна</a></li>
                        <li class="active">Стаклени Блокови</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('create-content')
    <br>
    <div class="container-fluid padding">
        <div class="row padding">
            @auth
                <div class="col-md-2 offset-10 float-right">
                    <a href="{{route("stakleni.create")}}" class="btn btn-info float-right">Креирај Стаклени Блокови</a>
                </div>
            @endauth

        </div>
        <br>
        <section class="">
            <div class="auto-container">
                <!--Sec Title-->

                <div class="row clearfix">
                    @if($stakleni->count())
                        <div class="sec-title centered">
                            <h2>Стаклени Блокови</h2>
                        </div>
                        <br>
                        <br>
                        @foreach($stakleni as $staklenis)
                        <!--Services Block-->
                            <div class="services-block col-md-4 col-sm-6 col-xs-12" style="margin-top: 25px;margin-bottom: 25px">
                                <div class="inner-box">
                                    <div class="image">
                                        <a href="{{url('stakleni/'.$staklenis->id)}}"><img
                                                src="{{asset('uploads/stakleni/'.$staklenis->image)}}" style="height: 400px;width: 400px; object-fit: cover"
                                                alt=""/></a>
                                    </div>
                                    <div class="lower-box">
                                        <div class="content">
                                            <a href="{{url('stakleni/'.$staklenis->id)}}" class="arrow-box"><span
                                                    class="fa fa-angle-right"></span></a>
                                            <div class="text">{{$staklenis->title}}</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="container-fluid padding">
                            <div class="col-md-12">
                                <h1 class="text-center">Во Моментов Нема Производи Во Оваа Категорија.</h1>
                            </div>
                            @endif
                        </div>
                        <div class="text-center">
                            {!! $stakleni->links()!!}

                        </div>
                </div>
            </div>
            <br>
            <br>

@endsection

