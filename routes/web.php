<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/Welcome', function () {
    return view('welcome');

});

Route::get('/', 'PagesController@getNewMain');

Route::resource('proizvodi','ProizvodiController');

Route::resource('pdf','PdfController');

Route::resource('galerija','GalleryController');

Route::resource('laminat','LaminatController');

Route::resource('stakleni','StakleniController');

Route::resource('lajsni','LajsniController');

Route::resource('ostanato','OstanatoController');

Route::resource('akcii','AkciiController');

Route::get('/informacii', 'PagesController@getInformacii');

Route::get('/contact', 'PagesController@getContact');

Route::post('/contact', 'PagesController@postContact');

Route::get('/sorabotka', 'PagesController@getSorabotka');

Route::post('/sorabotka', 'PagesController@postSorabotka');

Route::get('/template', 'PagesController@template');


Auth::routes(['register'=>true]);

Route::get('/home', 'HomeController@index')->name('home');


//STARI OVDEKA

Route::get('/main', 'PagesController@getMain');
