<?php

namespace App\Http\Controllers;

use App\Galerija;
use App\Http\Requests\CreateGalerijaRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Session;
use Illuminate\Support\Facades\File;
class GalleryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except'=>['index']]);
    }

    public function index()
    {
        $galerija = Galerija::all();
        return view('galerija.index', compact('galerija'));
    }


    public function create()
    {
        return view('galerija.create');
    }


    public function store(CreateGalerijaRequest $request)
    {
        $image = $request->file('image');
        $slug = str_slug($request->title);
        if (isset($image)) {
            $imagename = $slug . '.' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!file_exists('uploads/galerija')) {
                mkdir('uploads/galerija', 0777, true);
            }
            $image->move('uploads/galerija', $imagename);
        } else {
            $imagename = 'default.png';
        }

        $galerija = new Galerija;

        $galerija->title = $request->title;
        $galerija->image = $imagename;

        $galerija->save();

        Session::flash('success', 'Uspesno vnesuvajne');
        return redirect()->route('galerija.index');


    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $galerija = Galerija::find($id);

        return view("galerija.show")->with('galerija', $galerija);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $galerija = Galerija::find($id);

        if (file_exists('uploads/galerija/' . $galerija->image)) {
            unlink('uploads/galerija/' . $galerija->image);
        }

        $galerija->delete();
        Session::flash('success', 'Uspesno brisejne');

        return redirect()->route('galerija.index');
    }

}
