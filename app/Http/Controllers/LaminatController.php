<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateLaminatRequest;
use App\Http\Requests\UpdateLaminatRequest;
use App\Laminat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use App\Filters\Laminat\LaminatFilters;
use Session;

class LaminatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    public function index(Request $request)
    {
        $laminat = Laminat::orderBy('id', 'desc')->filter($request)->paginate(3);

        return view('laminat.index', compact('laminat'));
    }

    public function create()
    {
        return view('laminat.create');
    }

    public function store(CreateLaminatRequest $request)
    {
        $image = $request->file('image');

        $slug = str_slug($request->title);

        if (isset($image)) {
            $imagename = $slug . '.' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!file_exists('uploads/laminat')) {
                mkdir('uploads/laminat', 0777, true);
            }
            $image->move('uploads/laminat', $imagename);
        } else {
            $imagename = 'default.png';
        }

        $laminat = new Laminat;

        $laminat->title = $request->title;
        $laminat->sistemnagreejne = $request->sistemnagreejne;
        $laminat->debelina = $request->debelina;
        $laminat->klasanaotpornost = $request->klasanaotpornost;
        $laminat->boja = $request->boja;

        $laminat->image = $imagename;
        $laminat->save();

        Session::flash('success', 'Uspesno vnesuvajne');
        return redirect()->route('laminat.show', $laminat->id);

    }

    public function show($id)
    {
        $laminat = Laminat::find($id);

        return view('laminat.show', compact('laminat'));
    }

    public function edit($id)
    {
        $laminat = Laminat::find($id);

        return view('laminat.edit', compact('laminat'));

    }

    public function update(UpdateLaminatRequest $request, $id)
    {
        $laminat = Laminat::find($id);

        $image = $request->file('image');

        $slug = str_slug($request->title);

        if (isset($image)) {
            $imagename = $slug . '.' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!file_exists('uploads/laminat')) {
                mkdir('uploads/laminat', 0777, true);
            }
            unlink('uploads/laminat/' . $laminat->image);
            $image->move('uploads/laminat', $imagename);
        } else {
            $imagename = $laminat->image;
        }

        $laminat->title = $request->input('title');
        $laminat->sistemnagreejne = $request->input('sistemnagreejne');
        $laminat->debelina = $request->input('debelina');
        $laminat->klasanaotpornost = $request->input('klasanaotpornost');
        $laminat->boja = $request->input('boja');
        $laminat->boja = $request->input('boja');
        $laminat->image=$imagename;

        $laminat->save();

        Session::flash('success', 'Uspesno editirajne');

        return redirect()->route('laminat.show', $laminat->id);

    }


    public function destroy($id)
    {
        $laminat = Laminat::find($id);
        if (file_exists('uploads/laminat/' . $laminat->image)) {
            unlink('uploads/laminat/' . $laminat->image);
        }
        $laminat->delete();

        Session::flash('success', 'Uspesno brisejne');

        return redirect()->route('laminat.index');
    }
}
