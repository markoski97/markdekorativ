<?php

namespace App\Http\Controllers;


use App\Http\Requests\CreateProizvodiRequest;
use App\Http\Requests\UpdateProizvodiRequest;
use App\Proizvodi;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
Use Session;
class ProizvodiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except'=>['index']]);
    }

    public function index()
    {
        $proizvodi=Proizvodi::all();

        return view('proizvodi.index', compact('proizvodi'));

    }

    public function create()
    {
    return view('proizvodi.create');
    }

    public function store(CreateProizvodiRequest $request)
    {
        $slug = str_slug($request->title);

        $image = $request->file('image');

        if (isset($image)) {
            $imagename = $slug . '.' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!file_exists('uploads/proizvodi')) {
                mkdir('uploads/proizvodi', 0777, true);
            }
            $image->move('uploads/proizvodi', $imagename);
        } else {
            $imagename = 'default.png';
        }
        $proizvodi=new Proizvodi;

        $proizvodi->title=$request->title;
        $proizvodi->body=$request->body;
        $proizvodi->image=$imagename;
        $proizvodi->save();

        Session::flash('success','Uspesno vnesuvajne');
        return redirect()->route('proizvodi.show',$proizvodi->id);

    }

    public function show($id)
    {
        $proizvodi=Proizvodi::find($id);


        return view('proizvodi.show')->with('proizvodi',$proizvodi);
    }

    public function edit($id)
    {
        $proizvodi=Proizvodi::find($id);

        return view('proizvodi.edit',compact('proizvodi'));
    }

    public function update(UpdateProizvodiRequest $request, $id)
    {
        $proizvodi=Proizvodi::find($id);

        $proizvodi->title=$request->input('title');

        $proizvodi->body=$request->input('body');

        $image = $request->file('image');

        $slug = str_slug($request->title);

        if (isset($image)) {
            $imagename = $slug . '.' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!file_exists('uploads/proizvodi')) {
                mkdir('uploads/proizvodi', 0777, true);
            }
            unlink('uploads/proizvodi/' . $proizvodi->image);
            $image->move('uploads/proizvodi', $imagename);
        } else {
            $imagename = $proizvodi->image;
        }

        $proizvodi->image=$imagename;

        $proizvodi->save();

        Session::flash('success','Uspesno editirajne');

        return redirect()->route('proizvodi.show',$proizvodi->id);

    }

    public function destroy($id)
    {
        $proizvodi=Proizvodi::find($id);
        if (file_exists('uploads/proizvodi/' . $proizvodi->image)) {
            unlink('uploads/proizvodi/' . $proizvodi->image);
        }
        $proizvodi->delete();

        Session::flash('success','Uspesno brisejne');

        return redirect()->route('proizvodi.index');
    }
}
