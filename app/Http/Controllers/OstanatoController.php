<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateOstanatoRequest;
use App\Http\Requests\UpadeteOstanatoRequest;
use App\Ostanato;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Session;

class OstanatoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $ostanato = Ostanato::orderBy('id', 'desc')->paginate(24);

        return view('ostanato.index', compact('ostanato'));
    }

    public function create()
    {
        return view('ostanato.create');
    }

    public function store(CreateOstanatoRequest $request)
    {
        $image = $request->file('image');

        $slug = str_slug($request->title);

        if (isset($image)) {
            $imagename = $slug . '.' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!file_exists('uploads/ostanato')) {
                mkdir('uploads/ostanato', 0777, true);
            }
            $image->move('uploads/ostanato', $imagename);
        } else {
            $imagename = 'default.png';
        }


        $ostanato = new Ostanato;

        $ostanato->title = $request->title;
        $ostanato->body = $request->body;
        $ostanato->image = $imagename;
        $ostanato->save();

        Session::flash('success', 'Uspesno vnesuvajne');
        return redirect()->route('ostanato.show', $ostanato->id);

    }

    public function show($id)
    {
        $ostanato = Ostanato::find($id);

        return view('ostanato.show')->with('ostanato', $ostanato);
    }

    public function edit($id)
    {
        $ostanato = Ostanato::find($id);

        return view('ostanato.edit', compact('ostanato'));
    }

    public function update(UpadeteOstanatoRequest $request, $id)
    {
        $ostanato = Ostanato::find($id);

        $image = $request->file('image');

        $slug = str_slug($request->title);

        if (isset($image)) {
            $imagename = $slug . '.' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!file_exists('uploads/ostanato')) {
                mkdir('uploads/ostanato', 0777, true);
            }
            unlink('uploads/ostanato/' . $ostanato->image);
            $image->move('uploads/ostanato', $imagename);
        } else {
            $imagename = $ostanato->image;
        }

        $ostanato->title = $request->input('title');
        $ostanato->body = $request->input('body');
        $ostanato->image = $imagename;

        $ostanato->save();

        Session::flash('success', 'Uspesno editirajne');

        return redirect()->route('ostanato.show', $ostanato->id);

    }


    public function destroy($id)
    {
        $ostanato = Ostanato::find($id);
        if (file_exists('uploads/ostanato/' . $ostanato->image)) {
            unlink('uploads/ostanato/' . $ostanato->image);
        }
        $ostanato->delete();

        Session::flash('success', 'Uspesno brisejne');

        return redirect()->route('ostanato.index');
        //
    }
}
