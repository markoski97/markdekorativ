<?php

namespace App\Http\Controllers;

use App\Lajsni;
use Illuminate\Http\Request;
use Session;

class LajsniController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }
    public function index()
    {
       $lajsni = Lajsni::orderBy('id', 'desc')->paginate(6);

        return view('lajsni.index', compact('lajsni'));
    }
    public function create()
    {
        return view('lajsni.create');
    }
    public function store(Request $request)
    {
        $image = $request->file('image');

        $slug = str_slug($request->title);

        if (isset($image)) {
            $imagename = $slug . '.' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!file_exists('uploads/lajsni')) {
                mkdir('uploads/lajsni', 0777, true);
            }
            $image->move('uploads/lajsni', $imagename);
        } else {
            $imagename = 'default.png';
        }

        $lajsni=new Lajsni;
        $lajsni->title = $request->title;
        $lajsni->body = $request->body;
        $lajsni->type = $request->type;
        $lajsni->image = $imagename;
        $lajsni->save();

        Session::flash('success', 'Uspesno vnesuvajne');
        return redirect()->route('lajsni.index');

    }
    public function show($id){

        $lajsni=Lajsni::find($id);

        return view('lajsni.show',compact('lajsni'));

    }


    public function destroy($id)
    {
        $lajsni = Lajsni::find($id);
        if (file_exists('uploads/lajsni/' . $lajsni->image)) {
            unlink('uploads/lajsni/' . $lajsni->image);
        }
        $lajsni->delete();

        Session::flash('success', 'Uspesno brisejne');

        return redirect()->route('lajsni.index');
    }
}
