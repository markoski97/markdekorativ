<?php

namespace App\Http\Controllers;
use App\Http\Requests\SorabokaRequest;
use App\Mail\ContactTnx;
use App\Mail\NewContact;
use App\Mail\NewSorabotka;
use App\Mail\SorabotkaTnx;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ContactRequest;
use App\Akcija;


use Session;

class PagesController extends Controller
{

    public function getContact(){
        return view('pages.contact');
    }
    public function postContact(ContactRequest $request){

        $data=array(
          'email'=> $request->email,
            'subject'=> $request->subject,
            'bodymessage'=> $request->message
        );

        Mail::to('markoski97@gmail.com')->send(new NewContact($request));
        Mail::to($request->email)->send(new ContactTnx());
        Session::flash('success','Your mail was Sent!');
        return redirect('/contact');
    }

    public function template(){

        return view('NewDesign.template');
    }

    public function getNewMain(){
        $akcii=Akcija::orderBy('id','desc')->paginate(3);

        return view('NewDesign.pocetna',compact('akcii'));
    }

    public function getInformacii(){

        return view('pages.informacii');
    }

//STARITE
    public function getMain(){

        return view('main');
    }

    public function getSorabotka(){
        return view('/');
    }
    public function postSorabotka(SorabokaRequest $request){

        $data=array(
            'imefirma'=>'required',
            'email'=> $request->email,
            'bodymessage'=> $request->message,
            'file' =>$request->file,
        );
        Mail::send(new NewSorabotka($request),$data,function ($message) use ($data){
            $message->attach($data['file']->getRealPath(),array(
               'as' => 'file.'.$data['file']->getClientOriginalExtension(),
               'mime' =>$data['file']->getMimeType()
            ));
        });

        Mail::to($request->email)->send(new SorabotkaTnx());
        Session::flash('success','Your mail was Sent!');
        return redirect('/');
    }
}
