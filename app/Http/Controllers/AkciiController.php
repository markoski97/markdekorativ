<?php

namespace App\Http\Controllers;

use App\Akcija;
use App\Http\Requests\CreateAkciiRequest;
use App\Http\Requests\UpdateAkciiRequest;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Session;

class AkciiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $akcii = Akcija::orderBy('id', 'desc')->paginate(6);

        return view('akcii.index', compact('akcii'));
    }

    public function create()
    {
        return view('akcii.create');
    }

    public function store(CreateAkciiRequest $request)
    {

        $image = $request->file('image');

        $slug = str_slug($request->title);

        if (isset($image)) {
            $imagename = $slug . '.' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!file_exists('uploads/akcii')) {
                mkdir('uploads/akcii', 0777, true);
            }
            $image->move('uploads/akcii', $imagename);
        } else {
            $imagename = 'default.png';
        }


        $akcii = new Akcija;
        $akcii->title = $request->title;
        $akcii->image = $imagename;
        $akcii->body = $request->body;
        $akcii->price = $request->price;

        $akcii->save();

        Session::flash('success', 'Uspesno vnesuvajne');
        return redirect()->route('akcii.show', $akcii->id);
    }

    public function show($id)
    {
        $akcii = Akcija::findOrFail($id);

        return view('akcii.show')->with('akcii', $akcii);
    }

    public function edit($id)
    {
        $akcii = Akcija::find($id);

        return view('akcii.edit', compact('akcii'));
    }

    public function update(UpdateAkciiRequest $request, $id)
    {
        $akcii = Akcija::find($id);

              $image = $request->file('image');

        $slug = str_slug($request->title);

        if (isset($image)) {
            $imagename = $slug . '.' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!file_exists('uploads/akcii')) {
                mkdir('uploads/akcii', 0777, true);
            }
            unlink('uploads/akcii/' . $akcii->image);
            $image->move('uploads/akcii', $imagename);
        } else {
            $imagename = $akcii->image;
        }

        $akcii->title = $request->input('title');
        $akcii->image = $imagename;
        $akcii->body = $request->input('body');
        $akcii->price = $request->input('price');


        $akcii->save();


        Session::flash('success', 'Uspesno editirajne');

        return redirect()->route('akcii.show', $akcii->id);

    }

    public function destroy($id)
    {
        $akcii = Akcija::find($id);

        if (file_exists('uploads/akcii/' . $akcii->image)) {
            unlink('uploads/akcii/' . $akcii->image);
        }
        $akcii->delete();

        Session::flash('success', 'Uspesno brisejne');

        return redirect()->route('proizvodi.index');
    }
}
