<?php

namespace App\Http\Controllers;

use App\Stakleni;
use Illuminate\Http\Request;
use Session;

class StakleniController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    public function index()
    {
        $stakleni = Stakleni::orderBy('id', 'desc')->paginate(6);

        return view('stakleni.index', compact('stakleni'));
    }


    public function create()
    {
        return view('stakleni.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'image'=>'required|mimes:jpeg,jpg,bmp,png',
        ]);
        $image = $request->file('image');

        $slug = str_slug($request->title);

        if (isset($image)) {
            $imagename = $slug . '.' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!file_exists('uploads/stakleni')) {
                mkdir('uploads/stakleni', 0777, true);
            }
            $image->move('uploads/stakleni', $imagename);
        } else {
            $imagename = 'default.png';
        }


        $stakleni = new Stakleni();

        $stakleni->title = $request->title;
        $stakleni->body = $request->body;
        $stakleni->image = $imagename;
        $stakleni->save();

        Session::flash('success', 'Uspesno vnesuvajne');
        return redirect()->route('stakleni.show', $stakleni->id);
    }


    public function show($id)
    {
        $stakleni = Stakleni::find($id);

        return view('stakleni.show')->with('stakleni', $stakleni);
    }


    public function edit($id)
    {
        $stakleni = Stakleni::find($id);

        return view('stakleni.edit')->with('stakleni', $stakleni);
    }



    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'title'=>'required',
            'image'=>'required|mimes:jpeg,jpg,bmp,png',
        ]);

        $stakleni = Stakleni::find($id);

        $image = $request->file('image');

        $slug = str_slug($request->title);

        if (isset($image)) {
            $imagename = $slug . '.' . uniqid() . '.' . $image->getClientOriginalExtension();
            if (!file_exists('uploads/stakleni')) {
                mkdir('uploads/stakleni', 0777, true);
            }
            unlink('uploads/stakleni/' . $stakleni->image);
            $image->move('uploads/stakleni', $imagename);
        } else {
            $imagename = $stakleni->image;
        }

        $stakleni->title = $request->input('title');
        $stakleni->body = $request->input('body');
        $stakleni->image = $imagename;

        $stakleni->save();

        Session::flash('success', 'Uspesno editirajne');

        return redirect()->route('stakleni.show', $stakleni->id);
    }

    public function destroy($id)
    {
        $stakleni = Stakleni::find($id);
        if (file_exists('uploads/stakleni/' . $stakleni->image)) {
            unlink('uploads/stakleni/' . $stakleni->image);
        }
        $stakleni->delete();

        Session::flash('success', 'Uspesno brisejne');

        return redirect()->route('stakleni.index');
    }
}
