<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePdfRequest;
use App\Http\Requests\UpdatePdfRequest;
use App\Pdf;
use Illuminate\Http\Request;
Use Illuminate\Support\Facades\Storage;
use Session;

class PdfController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except'=>['show']]);
    }

    public function index()
    {

    }

    public function create()
    {
     return view('kategorijapdf.create');
    }

    public function store(CreatePdfRequest $request)
    {
        $pdf = $request->file('pdf');
        $slug = str_slug($request->title);
        if (isset($pdf)) {
            $pdfname = $slug . '.' . uniqid() . '.' . $pdf->getClientOriginalExtension();
            if (!file_exists('uploads/pdf')) {
                mkdir('uploads/pdf', 0777, true);
            }
            $pdf->move('uploads/pdf', $pdfname);
        } else {
            $pdf = 'default.pdf';
        }

        $pdf=new Pdf();
        $pdf->title=$request->title;
        $pdf->pdf=$pdfname;

        $pdf->save();

        Session::flash('success','Uspesno vnesuvajne na pdf');
        return redirect()->route('pdf.show',$pdf->id);
    }

    public function show($id)
    {
       $pdf=Pdf::find($id);
        $link = asset('uploads/pdf/'.$pdf->pdf);
       return view('kategorijapdf.show',compact('link', 'pdf'));//->with(4'pdf',$pdf);
    }

    public function edit($id)
    {
        $pdf=Pdf::find($id);

        return view('kategorijapdf.edit',compact('pdf'));
    }

    public function update(UpdatePdfRequest $request, $id)
    {
        $pdf = Pdf::find($id);

        $pdf->title = $request->input('title');

        if ($request->hasFile('pdf')) {
            //todo:: update image
            $PdfPath = request('pdf')->store('PDF.uploads', 'public');

            $pdf->pdf = $PdfPath;
        }

        $pdf->save();
        Session::flash('success','Uspesno editirajne');

        return redirect()->route('pdf.show',$pdf->id);
    }

    public function destroy($id)
    {
        $pdf=Pdf::find($id);

        $pdfroute=( public_path("\storage/{$pdf->pdf}"));
        unlink($pdfroute);

        $pdf->delete();

        Session::flash('success','Uspesno brisejne');

        return redirect()->route('pdf.create');
    }
}
